" vim: set et sw=2 ts=2:
set encoding=utf-8

" no compatibility with vi
set nocompatible

if match($PATH, "/usr/pkg/bin")<0
  let $PATH="/usr/pkg/bin:".$PATH
endif

"" {{{1 plugin: startify
let g:startify_skiplist = [
      \ 'COMMIT_EDITMSG',
      \ 'bundle/.*/doc',
      \ ]

if has("win32")
  let g:startify_bookmarks = [
        \ { 'c': '~/.vim/vimrc' },
        \ { 'o': 'e:/users/qaw/Projects/org/meeting_notes.org' },
        \ { 'p': 'e:/users/qaw/Projects/org/plan.org' }
        \ ]
endif

"" {{{1 plugin: vim-slime
let g:slime_target = "tmux"

if has("win32") && executable('conemuc')
  let g:slime_target = "conemu"
endif


"" {{{1 plugin: vim-signify
" only use signify with local scm
if has("win32")
  let g:signify_vcs_list = [ 'git' ]
else
  let g:signify_vcs_list = [ 'git', 'hg', 'svn' ]
endif


"" {{{1 plugin: ack.vim
let g:ack_use_dispatch=0
" ack uses ag or rg
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif
if executable('rg')
  let g:ackprg = 'rg --vimgrep'
endif
" Don't jump to first match
cnoreabbrev Ack Ack!

"" {{{1 plugin: vim-easy-align
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

"" {{{1 plugin: vim-ocaml
" merlin for ocaml development
if !has("win32")
  let opamshare = substitute(system('opam config var share'),'\n$','','''')
  if isdirectory(opamshare)
    let merlindir = opamshare . "/merlin/vim"
    if isdirectory(merlindir)
      execute "set runtimepath+=" . merlindir
    endif

    let opamsharevim = opamshare . "/vim"
    if isdirectory(opamsharevim)
      execute "set runtimepath+=" . opamsharevim
      execute "runtime! " . "syntax/ocp-indent.vim"
    endif
  endif
endif

"" {{{1 plugin: ctrlp.vim
" set the module as ctrlp cwd
let g:ctrlp_root_markers = ['Imakefile.mk', 'IdentityCard']
let g:ctrlp_custom_ignore = {
      \ 'dir':   '\v[\/](Objects)$',
      \ 'file': '\v\.(beam|so|dll)$',
      \ }
"" {{{1 plugin: ALE
" only enable for some types
let g:ale_linters_explicit = 1
let g:ale_linters = {
      \ 'rust': ['analyzer', 'cargo', 'rustc'],
      \ 'sh': ['shellcheck', 'bashate'],
      \ 'xml': ['xmllint'],
      \ }
if has("autocmd")
  augroup ale_defaults
    autocmd!
    autocmd FileType rust setlocal omnifunc=ale#completion#OmniFunc
    autocmd FileType rust map <buffer> <c-]> :ALEGoToDefinition<CR>
    autocmd FileType rust map <buffer> <c-K> <Plug>(ale_hover)
  augroup END
endif

"" {{{1 plugin: language server protocol
let g:lsp_settings_enable_suggestions = 0
let g:lsp_settings = {
\  'perl-languageserver': {
\    'disabled': 1,
\   }
\}

let g:lsp_diagnostics_enabled = 0
function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol-search)
    nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> [g <plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)
    inoremap <buffer> <expr><c-f> lsp#scroll(+4)
    inoremap <buffer> <expr><c-d> lsp#scroll(-4)

    let g:lsp_format_sync_timeout = 1000
    autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')

    " refer to doc to add more commands
endfunction

"" {{{1 plugin: tagbar
let Tlist_Ctags_Cmd = "exctags"

"" {{{1 plugin: vim-orgmode
if has("win32")
  let g:org_agenda_files = [
        \ 'e:/users/qaw/Projects/org/meeting_notes.org',
        \ 'e:/users/qaw/Projects/org/plan.org'
        \ ]
endif
function Org_agenda_open()
  if empty(g:org_agenda_files)
    echo "No agenda file defined: see g:org_agenda_files"
  else
    let first_agenda_file = g:org_agenda_files[0]
    :execute "edit +10 " . fnameescape(first_agenda_file)
  endif
endfunction
command OrgOpen call Org_agenda_open()

if has('localmap') && has('autocmd')
  augroup orgmode_group
    autocmd!
    autocmd FileType org inoremap <buffer> <localleader>sa <C-O>:py ORGMODE.plugins[u"Date"].insert_timestamp()<CR>
    autocmd FileType org inoremap <buffer> <localleader>st <C-O>:OrgSetTags<CR>
  augroup END
endif

"" {{{1 plugin: gundo.vim : mappend to <leader>gu
nnoremap <leader>gu :GundoToggle<CR>
let g:gundo_right = 1

"" {{{1 tip: <leader>sh : syntax highlighting groups for word under cursor
nnoremap <leader>sh :call <SID>SynStack()<CR>
function! <SID>SynStack()
  if !exists("*synstack")
    return
  endif
  echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc
"" }}}

" use syntax highlighting if possible
syntax on

" faster open for files with very long lines
set synmaxcol=300

" no beep, never
set t_vb=
set visualbell

" incremental search
set incsearch

" highlighting search
set hlsearch

" indentation depends on the filetype
filetype indent on
filetype plugin on

let mapleader=","
let malocalpleader='\'

" view matching parents
set showmatch

" Allow backspacing over everything in insert mode
set backspace=indent,eol,start

" header in :hardcopy : print on the default printer
set pheader=%<%f%h%m%40{strftime(\"%I:%M:%S\ \%p,\ %a\ %b\ %d,\ %Y\")}%=Page\ %N
" fold with {{{*****}}}
set foldmethod=marker

" the last line can be a modeline
set modeline
set modelines=5

" : has bash-like completion
" Allow backspacing over everything in insert mode
set backspace=indent,eol,start

" header in :hardcopy : print on the default printer
set pheader=%<%f%h%m%40{strftime(\"%I:%M:%S\ \%p,\ %a\ %b\ %d,\ %Y\")}%=Page\ %N
" fold with {{{*****}}}
set foldmethod=marker

" the last line can be a modeline
set modeline
set modelines=5

" : has bash-like completion
set wildmode=list:longest,full
set suffixes=.bak,~,.swp,.o,.info,
      \.aux,.log,.dvi,.bbl,.blg,.brf,
      \.cb,.ind,.idx,.ilg,.inx,.toc,
      \.out,.native

set laststatus=2

" vertical split look
set fillchars=vert:\ ,fold:-

" common abbrevs
iabbrev #n //{{{<CR>//}}}<UP>
iabbrev #m %<<<<CR>%>>><UP>

" swap and backup files, not in my working directory
if has("win32")
  set backupdir=$HOME/vimfiles/backup_files//
  set directory=$HOME/vimfiles/swap_files//
else
  set backupdir=$HOME/.vim/backup_files//
  set directory=$HOME/.vim/swap_files//
endif

set backup
set writebackup
if has('persistent_undo')
  set undofile
  if has("win32")
    set undodir=$HOME/vimfiles/undo_files//
  else
    set undodir=$HOME/.vim/undo_files//
  endif
endif

"{{{ Mappings
" <Leader>B      = break line at current position *and* join the next line
nmap <Leader>B r<CR>Vjgq

imap [5D <C-Left>
imap [5C <C-Right>

" use ,cd to change current working dir to where the buffer is
nnoremap <Leader>cd :lcd %:p:h<CR>:pwd<CR>

" CDBuffer change current working directory to that on the current buffer
command CDBuffer cd %:p:h

" buffer navigation
:nmap <C-j> :bprevious<cr>
:nmap <C-k> :bnext<cr>
:map  <C-j> :bprevious<cr>
:map  <C-k> :bnext<cr>
:imap <C-j> <ESC>:bprevious<cr>i
:imap <C-k> <ESC>:bnext<cr>i
:nmap <C-n> :tabnew<cr>
"}}}

"{{{ autocommands
if has("autocmd")
  augroup mail_group
    autocmd!
    " do not quote the signature when replying with mutt
    autocmd FileType mail silent! :/^> --\s\?$/,$d
  augroup END

  augroup tex_group
    autocmd!
    " use <<<,>>> in tex for folds
    autocmd FileType tex    setlocal foldmarker=<<<,>>>
    autocmd FileType tex    setlocal textwidth=78
  augroup END

  augroup xml_group
    autocmd!
    " syntax folding for xml is too slow to enable by default
    let g:xml_syntax_folding = 0
    autocmd FileType ant       setlocal foldmethod=syntax
    autocmd FileType ant       setlocal foldnestmax=3
    autocmd FileType ant       setlocal foldminlines=3
    autocmd FileType ant       setlocal foldlevel=4
    autocmd FileType xml       setlocal foldmethod=syntax
    autocmd FileType xml       setlocal foldlevel=4
    autocmd FileType ant,xhtml setlocal expandtab
    autocmd FileType ant,xhtml setlocal sw=2
    autocmd FileType xml       setlocal et sw=2 ts=2
  augroup END

  augroup indent_group
    autocmd!
    autocmd FileType gom,java,tom,tex,antlr setlocal expandtab sw=2 ts=2 nosmarttab
    autocmd FileType coq    setlocal expandtab sw=2 tabstop=2 nosmarttab autoindent
    autocmd FileType erlang setlocal tabstop=2 shiftwidth=2 expandtab
    autocmd FileType ocaml  setlocal tabstop=2 shiftwidth=2 expandtab
    autocmd FileType c      setlocal tabstop=8 shiftwidth=4 noexpandtab
    autocmd FileType make   setlocal ts=8 sw=8 noet
    autocmd BufEnter *.mql  setlocal ts=2 sw=2 et
  augroup END

  augroup tags_group
    autocmd!
    autocmd FileType tex          setlocal tags=./tags,tags
    autocmd FileType gom,java,tom setlocal tags=./tags,tags
  augroup END

  augroup custom_ft_group
    autocmd BufRead,BufNewFile *.md           setlocal filetype=markdown
    autocmd BufEnter *.mql                    setlocal filetype=sh
    colorscheme solarized
  augroup END
endif
"}}}

"{{{ gui
if has("win32")
  behave xterm
  set guifont=Inconsolata-dz\ for\ Powerline:h12
  let g:airline_powerline_fonts=1
  let g:airline#extensions#ale#enabled = 1
  autocmd GUIEnter * winpos 100 50
  autocmd GUIEnter * set lines=50 columns=90
  colorscheme molokai
elseif has("gui_gtk2")
  let g:airline_powerline_fonts=0
  set guifont=Inconsolata 12
  colorscheme molokai
elseif has("gui_gtk3")
  let g:airline_powerline_fonts=1
  let g:airline#extensions#ale#enabled = 1
  set guifont=Inconsolata\ 12
  colorscheme molokai
else
  set guifont=Consolas:h12
  let g:airline_powerline_fonts=0
  colorscheme gruvbox
endif
set guioptions-=T
set guioptions-=m
set guioptions-=r
set background=dark
set anti "antialias
"}}}

"{{{ no gui
if match($TERM_PROGRAM,"Apple_Terminal")==0
  let g:airline_powerline_fonts=1
  let g:airline#extensions#ale#enabled = 1
  let g:solarized_termcolors=256
  colorscheme solarized
  set background=light
elseif match($ConEmuANSI,"ON")==0
  set termencoding=utf8
  set term=xterm
  set t_Co=256
  let &t_AB="\e[48;5;%dm"
  let &t_AF="\e[38;5;%dm"
  inoremap <Char-0x008> <BS>
  nnoremap <Char-0x008> <BS>
  let g:airline_powerline_fonts=0
  let g:airline#extensions#ale#enabled = 1
  let g:solarized_termcolors=256
  colorscheme solarized
  let g:airline_theme='zenburn'
  set background=dark
elseif match($COLORTERM,"gnome-terminal")==0
  let g:airline_powerline_fonts=1
  let g:airline#extensions#ale#enabled = 1
  let g:solarized_termcolors=256
  colorscheme default
  set background=dark
elseif match($TERM_PROGRAM,"iTerm.app")==0
  let g:airline_powerline_fonts=1
  let g:airline#extensions#ale#enabled = 1
  let g:solarized_termcolors=256
  colorscheme default
  set background=dark
else
  colorscheme gruvbox
  set background=dark
endif
" allows cursor change in tmux mode
if exists('$TMUX')
  let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
  let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
  let &t_SI = "\<Esc>]50;CursorShape=1\x7"
  let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif
"}}}
